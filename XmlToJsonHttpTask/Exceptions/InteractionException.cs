﻿using System;

namespace XmlToJsonHttpTask.Exceptions
{
    class InteractionException : Exception
    {
        public InteractionException(string message)
            : base(message)
        {

        }
    }
}
