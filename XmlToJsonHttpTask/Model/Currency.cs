﻿using System;
using System.Xml.Serialization;

namespace XmlToJsonHttpTask.Model
{
    [Serializable]
    public class Currency
    {
        [XmlAttribute("id")]
        public string Id { get; set; }

        [XmlAttribute("rate")]
        public string Rate { get; set; }

        [XmlAttribute("plus")]
        public string Plus { get; set; }
    }
}
