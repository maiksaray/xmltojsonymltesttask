﻿using System;
using System.Xml.Serialization;

namespace XmlToJsonHttpTask.Model
{
 
    [Serializable]
    public class Category
    {
        [XmlAttribute("id")]
        public string Id { get; set; }
        
        [XmlText]
        public string Value { get; set; }

        [XmlAttribute("parentId")]
        public string ParentId { get; set; }
    }
}
