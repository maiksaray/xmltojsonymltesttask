﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace XmlToJsonHttpTask.Model
{
    [Serializable]
    public class Shop
    {
        [XmlElement("name")]
        public string Mame { get; set; }

        [XmlElement("company")]
        public string Company { get; set; }

        [XmlElement("url")]
        public string Url { get; set; }

        [XmlArray("currencies")]
        [XmlArrayItem("currency", typeof(Currency))]
        public List<Currency> Currencies { get; set; }

        [XmlArray("categories")]
        [XmlArrayItem("category", typeof(Category))]
        public List<Category> Categories { get; set; }

        [XmlElement("local_delivery_cost")]
        public string LocalDeliveryCost { get; set; }

        [XmlArray("offers")]
        [XmlArrayItem("offer", typeof(Offer))]
        public List<Offer> Offers { get; set; }
    }
}
