﻿using Newtonsoft.Json.Converters;

namespace XmlToJsonHttpTask.Converters
{
    public class JsonDateTimeConverter : IsoDateTimeConverter
    {
        public JsonDateTimeConverter()
        {
            base.DateTimeFormat = FormatSettings.Default.DateTimeFormat;
        }
    }
}
