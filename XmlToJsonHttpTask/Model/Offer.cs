﻿using System;
using System.Xml.Serialization;

namespace XmlToJsonHttpTask.Model
{
    [Serializable]
    public class Offer
    {
        [XmlAttribute("id")]
        public string Id { get; set; }

        [XmlAttribute("type")]
        public string Type { get; set; }

        [XmlAttribute("bid")]
        public string Bid { get; set; }

        [XmlAttribute("cbid")]
        public string Cbid { get; set; }

        [XmlAttribute("available")]
        public bool Available { get; set; }

        [XmlElement("url")]
        public string Url { get; set; }

        [XmlElement("price")]
        public string Price { get; set; }

        [XmlElement("currencyId")]
        public string CurrencyId { get; set; }

        [XmlElement("categoryId")]
        public string CategoryId { get; set; }

        [XmlElement("picture")]
        public string Picture { get; set; }

        [XmlElement("delivery")]
        public string Delivery { get; set; }

        [XmlElement("local_delivery_cost")]
        public string LocalDeliveryCost { get; set; }

        [XmlElement("typePrefix")]
        public string TypePrefix { get; set; }

        [XmlElement("vendor")]
        public string Vendor { get; set; }

        [XmlElement("vendorCode")]
        public string VendorCode { get; set; }

        [XmlElement("model")]
        public string Model { get; set; }

        [XmlElement("description")]
        public string Description { get; set; }

        [XmlElement("manufacturer_warranty")]
        public string ManufacturerWarranty { get; set; }

        [XmlElement("country_of_origin")]
        public string CountyOfOrigin { get; set; }
    }
}
