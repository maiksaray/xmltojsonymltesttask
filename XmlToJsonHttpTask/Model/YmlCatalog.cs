﻿using System;
using System.Globalization;
using System.Xml.Serialization;
using Newtonsoft.Json;
using XmlToJsonHttpTask.Converters;

namespace XmlToJsonHttpTask.Model
{
    [Serializable]
    [XmlRoot("yml_catalog")]
    public class YmlCatalog
    {
        private DateTime _date;

        [JsonIgnore]
        [XmlAttribute("date")]
        public string DateString { get; set; }

        [JsonProperty("date")]
        [JsonConverter(typeof(JsonDateTimeConverter))]
        public DateTime Date
        {
            get { 
                return _date != new DateTime() ? 
                    _date :
                    _date = DateTime.ParseExact(DateString, FormatSettings.Default.DateTimeFormat, CultureInfo.InvariantCulture); }
        }

        [XmlElement("shop")]
        [JsonProperty("shop")]
        public Shop Shop { get; set; }
    }
}
