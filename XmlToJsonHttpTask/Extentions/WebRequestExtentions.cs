﻿using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace XmlToJsonHttpTask.Extentions
{
    public static class WebRequestExtentions
    {
        public static async Task<WebResponse> GetResponseAsync(this WebRequest request, CancellationToken ct)
        {
            using (ct.Register(request.Abort, false))
            {
                var response = await request.GetResponseAsync();
                ct.ThrowIfCancellationRequested();
                return (HttpWebResponse)response;
            }
        }
    }
}
