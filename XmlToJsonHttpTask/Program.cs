﻿using System;
using System.Threading;
using System.Threading.Tasks;
using XmlToJsonHttpTask.Network;

namespace XmlToJsonHttpTask
{
    class Program
    {
        //todo: maybe move to settings
        private const string XmlUrl = "http://partner.market.yandex.ru/pages/help/YML.xml";
        private const string OfferId = "12344";
        private const string OfferUrl = "http://localhost:80/offers/";
        

        //todo:maybe switch all ids to int type, but there is no info that ids are only ints

        static void Main(string[] args)
        {
            try
            {
                Task.Run(async () =>
                {
                    Console.WriteLine("Getting catalog at " + XmlUrl);
                    
                    var catalog = await HttpService.ObtainCatalogAsync(XmlUrl,CancellationToken.None);
                    Console.WriteLine("Catalog obtained, "+catalog.Shop.Offers.Count+" offers found");
                    
                    var offer = catalog.Shop.Offers.Find(o => o.Id == OfferId);

                    Console.WriteLine("Offer selected: "+offer.Description);

                    var text = await HttpService.SendOfferJsonAsync(
                        offer,
                        OfferUrl, 
                        CancellationToken.None);

                    Console.WriteLine("Received text in response: '" + text + "'");
                }).Wait();
            }
            catch (Exception e)
            {
                //Logging
                Console.WriteLine("something went wrong");
            }
        }
    }
}
