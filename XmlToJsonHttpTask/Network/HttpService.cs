﻿using System;
using System.IO;
using System.Net;
using System.Runtime.Serialization;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Newtonsoft.Json;
using XmlToJsonHttpTask.Exceptions;
using XmlToJsonHttpTask.Extentions;
using XmlToJsonHttpTask.Model;

namespace XmlToJsonHttpTask.Network
{
    class HttpService
    {
        /// <summary>
        /// basic method for simple http interaction (asynchronious)
        /// </summary>
        /// <param name="url">url to request</param>
        /// <param name="request">data to send in request body</param>
        /// <param name="method">http-method to use</param>
        /// <param name="ct">token for cancelling async task</param>
        /// <returns></returns>
        private static async Task<string> RequestAsync(string url, string request, string method, CancellationToken ct)
        {
            try
            {
                var httpRequest = WebRequest.CreateHttp(new Uri(url));
                httpRequest.Method = method;
                httpRequest.ContentType = "application/json";
                var encoding = Encoding.GetEncoding("utf-8");

                var bytedata = encoding.GetBytes(request);

                httpRequest.ContentLength = bytedata.Length;
                using (var requestStream = httpRequest.GetRequestStream())
                {
                    requestStream.Write(bytedata, 0, bytedata.Length);
                }

                using (var response = await httpRequest.GetResponseAsync(ct))
                {
                    using (var stream = response.GetResponseStream())
                    {
                        using (var reader = new StreamReader(stream, Encoding.UTF8))
                        {
                            return reader.ReadToEnd();
                        }
                    }
                }
            }
            catch (UriFormatException e)
            {
                throw new InteractionException("invalid url format set in settings");
            }
            catch (WebException e)
            {
                throw new InteractionException("network error");
            }
            catch (SerializationException e)
            {
                throw new InteractionException("unexpected data structure, can't read xml");
            }
            catch (Exception e)
            {
                //log
                throw new InteractionException("unexpected error, please contact developers");
            }
        }

        /// <summary>
        /// Gets catalog from specified url and parses it from xml asynchroniously
        /// </summary>
        /// <param name="xmlUrl">url to request for catalog</param>
        /// <param name="ct">token for cancellation async task</param>
        /// <returns></returns>
        public static async Task<YmlCatalog> ObtainCatalogAsync(string xmlUrl, CancellationToken ct)
        {
            try
            {
                var request = WebRequest.CreateHttp(new Uri(xmlUrl));
                request.Method = "GET";

                using (var response = await request.GetResponseAsync(ct))
                {
                    using (var stream = response.GetResponseStream())
                    {
                        using (
                            var reader = new StreamReader(stream,
                                Encoding.GetEncoding(FormatSettings.Default.XmlEncoding)))
                        //bad, but fast solution for getting right encoding (but independent from xml encoding)
                        {
                            var serializer = new XmlSerializer(typeof(YmlCatalog));
                            return (YmlCatalog)serializer.Deserialize(reader);
                        }
                    }
                }
            }
            catch (UriFormatException e)
            {
                throw new InteractionException("invalid url format set in settings");
            }
            catch (WebException e)
            {
                throw new InteractionException("network error");
            }
            catch (SerializationException e)
            {
                throw new InteractionException("unexpected data structure, can't read xml");
            }
            catch (Exception e)
            {
                //log
                throw new InteractionException("unexpected error, please contact developers");
            }
        }

        /// <summary>
        /// sends specified offer to specified url using POST in json format asynchroniously
        /// </summary>
        /// <param name="offer">offer to send</param>
        /// <param name="url">url to POST to</param>
        /// <param name="ct">token for cancelling async task</param>
        /// <returns></returns>
        public static async Task<string> SendOfferJsonAsync(Offer offer, string url, CancellationToken ct)
        {
            try
            {
                return await RequestAsync(url, JsonConvert.SerializeObject(offer), "POST", ct);
            }
            catch (JsonException e)
            {
                throw new InteractionException("unexpected data structure, can't write to json");
            }
        }
    }
}
